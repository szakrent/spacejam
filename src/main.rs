mod asteroid;
mod radar;
mod ship;
mod gui;

extern crate ggez;
#[macro_use]
extern crate gfx;
extern crate nalgebra;

use ggez::event::{self, Axis, Button, Keycode, Mod, MouseButton, MouseState};
use ggez::graphics::*;
use ggez::*;
use radar::radar_renderer::RadarRenderer;
use std::env;
use std::f64::consts::PI;
use std::path;
use std::rc::Rc;
use std::rc::Weak;
use std::time::{Duration, Instant};

const UPDATES_PER_SECOND: f32 = 150.0;
const MILLIS_PER_UPDATE: u64 = (1.0 / UPDATES_PER_SECOND * 1000.0) as u64;

struct GameState {
    ppi_renderer: radar::radar_renderer::RadarRenderer,
    b_renderer: radar::radar_renderer::RadarRenderer,
    last_update: Instant,
    time_error: Duration,
    b_radar_counter: f64,
    b_radar: radar::Radar,
    ppi_radar: radar::Radar,
    asteroids: Vec<Rc<asteroid::Asteroid>>,
    gui: gui::Gui,
    //target: nalgebra::Vector2<f64>,
    frames: usize,
    ship: ship::Ship,
}

impl GameState {
    fn new(ctx: &mut Context) -> GameResult<GameState> {
        Ok(GameState {
            frames: 0,
            asteroids: vec![
                Rc::new(asteroid::Asteroid::new(
                    nalgebra::Vector2::new(-600.0, 0.0),
                    200.0,
                )),
                Rc::new(asteroid::Asteroid::new(
                    nalgebra::Vector2::new(-600.0, 200.0),
                    300.0,
                )),
                Rc::new(asteroid::Asteroid::new(
                    nalgebra::Vector2::new(600.0, 0.0),
                    200.0,
                )),
                Rc::new(asteroid::Asteroid::new(
                    nalgebra::Vector2::new(0.0, -600.0),
                    200.0,
                )),
                Rc::new(asteroid::Asteroid::new(
                    nalgebra::Vector2::new(0.0, 600.0),
                    200.0,
                )),
            ],
            b_radar_counter: 0.0,
            ppi_radar: radar::Radar::new(
                nalgebra::Vector2::new(0.0, 0.0),
                radar::Antenna::new(0.0, 8.0),
                1e6,
                1e-9,
                1e9,
            ),
            b_radar: radar::Radar::new(
                nalgebra::Vector2::new(0.0, 0.0),
                radar::Antenna::new(0.0, 8.0),
                1e6,
                1e-9,
                1e9,
            ),
            gui: gui::Gui::new(ctx)?,
            last_update: Instant::now(),
            time_error: Duration::from_millis(0),
            b_renderer: radar::radar_renderer::RadarRenderer::new(ctx, 1000.0, -0.8, 0.8, false)?,
            ppi_renderer: radar::radar_renderer::RadarRenderer::new(ctx, 1000.0, -1.0, 1.0, true)?,
            ship: ship::Ship::new(nalgebra::Vector2::new(0f64, 0f64)),
        })
    }
    fn parse_input(&mut self, keycode: Keycode, state: bool) {
        use ggez::event::Keycode::*;
        use ship::Direction;
        use ship::Direction::*;
        match keycode {
            W => self.ship.switch_direction(Forward, state),
            S => self.ship.switch_direction(Backward, state),
            A => self.ship.switch_direction(Direction::Left, state),
            D => self.ship.switch_direction(Direction::Right, state),
            Q => self.ship.switch_direction(TurnLeft, state),
            E => self.ship.switch_direction(TurnRight, state),
            _ => {}
        };
    }
}

impl event::EventHandler for GameState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        let dt = Instant::now() - self.last_update;
        self.last_update = Instant::now();
        self.time_error += dt;

        while self.time_error >= Duration::from_millis(MILLIS_PER_UPDATE) {
            self.time_error -= Duration::from_millis(MILLIS_PER_UPDATE);

            self.ship
                .update(std::time::Duration::from_millis(MILLIS_PER_UPDATE));

            for asteroid in &self.asteroids {
                self.ship.check_collision(asteroid.position, asteroid.radius);
            }

            let mut targets: Vec<Rc<dyn radar::RadarTarget>> = vec![];

            for asteroid in &self.asteroids {
                targets.push(Rc::clone(asteroid) as std::rc::Rc<radar::RadarTarget>);
            }

            self.b_radar.set_position(self.ship.position);
            self.ppi_radar.set_position(self.ship.position);
            self.b_radar.antenna.mount_direction = self.ship.rotation;
            self.ppi_radar.antenna.mount_direction = self.ship.rotation;

            self.ppi_radar.make_mesurement(&targets, 1e3);
            self.b_radar.make_mesurement(&targets, 1e3);

            self.ppi_radar.antenna.rotate(0.03);
            self.b_radar.antenna.direction = self.b_radar_counter.sin()*0.8;
            self.b_radar_counter += 0.025;


            if let Some(ref measurement) = self.ppi_radar.last_mesurement {
                self.ppi_renderer.update(ctx, &measurement, &self.ship)?;
            }
            if let Some(ref measurement) = self.b_radar.last_mesurement {
                self.b_renderer.update(ctx, &measurement, &self.ship)?;
            }
            self.last_update = Instant::now();
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        self.ppi_renderer
            .render_radar(ctx, Rect::new(0.0, 0.0, 400.0, 400.0))?;
        self.b_renderer
            .render_radar(ctx, Rect::new(0.0, 400.0, 600.0, 200.0))?;

        self.gui.render(ctx, &self.ship);

        graphics::present(ctx);

        self.frames += 1;
        if (self.frames % 100) == 0 {
            println!("FPS: {}", ggez::timer::get_fps(ctx));
        }

        timer::yield_now();
        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, keycode: Keycode, keymod: Mod, repeat: bool) {
        self.parse_input(keycode, true);
    }
    fn key_up_event(&mut self, _ctx: &mut Context, keycode: Keycode, keymod: Mod, repeat: bool) {
        self.parse_input(keycode, false);
    }
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut ContextBuilder::new("Space radar", "ZSA")
        .window_setup(conf::WindowSetup::default().title("Space radar"))
        .window_mode(conf::WindowMode::default().dimensions(600, 600))
        .build()
        .expect("Failed to create context!");
    graphics::set_background_color(ctx, [1.0*0.10, 0.75*0.11, 0.0, 1.0].into());

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        ctx.filesystem.mount(&path, true);
    }

    let state = &mut GameState::new(ctx).unwrap();
    if let Err(e) = event::run(ctx, state) {
        println!("Error while running game: {}", e);
    } else {
        println!("Game exited sucesfully!");
    }
}
