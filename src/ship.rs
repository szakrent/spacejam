extern crate nalgebra;

use std::f64::consts::PI;

#[derive(Debug)]
pub struct Ship {
    pub health: u32,
    pub radius: f64,
    pub mass: f64,                          //kg
    pub inertia: f64,                       //kg * m^2
    pub position: nalgebra::Vector2<f64>,   //m
    pub rotation: f64,                      //radians
    pub l_velocity: nalgebra::Vector2<f64>, // m/s
    pub a_velocity: f64,                    // radians/s
    pub engines: Vec<Engine>,
}

impl Ship {
    pub fn new(position: nalgebra::Vector2<f64>) -> Ship {
        let mut ship = Ship {
            mass: 200f64,
            inertia: 50000f64,
            health: 5,
            radius: 15.0,
            position,
            rotation: 0f64,
            l_velocity: nalgebra::Vector2::new(0f64, 0f64),
            a_velocity: 0f64,
            engines: Vec::new(),
        };
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(0f64, -2f64),
            angle: super::std::f64::consts::PI * 3f64 / 2f64,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::Left,
            name: "Bottom",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(0f64, 2f64),
            angle: super::std::f64::consts::PI / 2f64,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::Right,
            name: "Top",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(1f64, 1f64),
            angle: 0f64,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::TurnRight,
            name: "Right top",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(1f64, -1f64),
            angle: 0f64,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::TurnLeft,
            name: "Rigth bottom",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(-1f64, 1f64),
            angle: super::std::f64::consts::PI,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::TurnRight,
            name: "Left top",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(-1f64, -1f64),
            angle: super::std::f64::consts::PI,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::TurnLeft,
            name: "Left bottom",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(1f64, 0f64),
            angle: 0f64,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::Forward,
            name: "Rigth",
        });
        ship.engines.push(Engine {
            position: nalgebra::Point2::new(1f64, 0f64),
            angle: super::std::f64::consts::PI,
            force: 3000f64,
            throttle: 0f64,
            direction: Direction::Backward,
            name: "Left",
        });
        ship
    }

    pub fn update(&mut self, delta_time: super::std::time::Duration) {
        use self::nalgebra::Vector2;

        let mut l_force = nalgebra::Vector2::new(0.0f64, 0.0f64);
        let mut a_force = 0.0f64;
        for engine in &self.engines {
            let l_angle = self.rotation + engine.angle;
            let l_length = engine.force * engine.throttle;
            l_force += Vector2::new(l_angle.cos() * l_length, l_angle.sin() * l_length);

            let a_length = engine.force
                * engine.throttle
                * (engine.position.x.powi(2) + engine.position.y.powi(2)).sqrt();
            let a_angle = super::std::f64::consts::PI
                - (engine.position.y / engine.position.x).atan()
                + engine.angle;
            a_force += a_length * a_angle.sin()
        }
        let l_acceleration = l_force / self.mass;
        let a_acceleration = a_force / self.inertia;

        self.position += self.l_velocity * (delta_time.subsec_micros() as f64 / 1000000f64);
        self.rotation += self.a_velocity * (delta_time.subsec_micros() as f64 / 1000000f64);
        if(self.rotation > PI){
            self.rotation -= 2.0*PI;
        }
        if(self.rotation < -PI){
            self.rotation += 2.0*PI;
        }
        
        self.l_velocity += l_acceleration * (delta_time.subsec_micros() as f64 / 1000000f64);
        self.a_velocity += a_acceleration * (delta_time.subsec_micros() as f64 / 1000000f64);
    }
    pub fn switch_direction(&mut self, direction: Direction, state: bool) {
        for engine in &mut self.engines {
            if engine.direction == direction {
                engine.throttle = if state { 1f64 } else { 0f64 };
            }
        }
    }
    pub fn check_collision(&mut self, position: nalgebra::Vector2<f64>, radius: f64){
        let collision_vec = self.position-position;
        let distance = collision_vec.norm();
        if distance < self.radius+radius {
            if self.health > 0 {
                self.health -= 1;
            }
            self.l_velocity *= -0.8;
            self.position += collision_vec*((radius+self.radius)/distance-1.0);
        }
    }
}

#[derive(Debug)]
pub struct Engine {
    position: nalgebra::Point2<f64>, //m
    angle: f64,                      //radians
    force: f64,                      //newtons
    throttle: f64,                   //0 - min, 1 - max
    direction: Direction,
    name: &'static str,
}

#[derive(Debug, PartialEq)]
pub enum Direction {
    Left,
    Right,
    Forward,
    Backward,
    TurnLeft,
    TurnRight,
    None,
}
