use ggez::*;
use ggez::graphics::*;
use ship::Ship;
use ggez::graphics::Font::*;

pub struct Gui{
    font: Font,
}

impl Gui{
    pub fn new(ctx: &mut Context) -> GameResult<Gui> {
        Ok(Gui{
            font: Font::new_glyph_font(ctx, "/VGA437.ttf")?,
        })
    }
    pub fn render(&self, ctx: &mut Context, player: &Ship) -> GameResult<()> {
        let mut health_indicator = String::new();
        health_indicator += "[";
        for i in 1..6{
            if(i > player.health){
                health_indicator += "-";
            }
            else{
                health_indicator += "#";
            }
        }
        health_indicator += "]";

        let mut text_cached: Vec<TextCached> = Vec::new();
        
        let color = Some(Color::new(0.8, 0.60, 0.0, 1.0));
        let scale = Some(Scale::uniform(20.0));
        if let GlyphFont(font_id) = self.font {
            text_cached.push(TextCached::new(TextFragment{text: "----SHIP DATA----".to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("HEALTH: {}", health_indicator).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: "--VELOCITY DATA--".to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("VELX: {:+09.3}", player.l_velocity.x).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("VELY: {:+09.3}", player.l_velocity.y).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("VELA: {:+09.3}", player.a_velocity).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: "--POSITION DATA--".to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("POSX: {:+09.3}", player.position.x).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("POSY: {:+09.3}", player.position.y).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("POSA: {:+09.3}", player.rotation).to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: "---TARGET DATA---".to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: "--DISPLAY  DATA--".to_string(), color, scale, font_id: Some(font_id)})?); 
            text_cached.push(TextCached::new(TextFragment{text: format!("FPS:  {:+09.3}", timer::get_fps(ctx)).to_string(), color, scale, font_id: Some(font_id)})?); 
        }
        

        let mut offset = 10.0;
        for text in text_cached {
            text.queue(ctx, Point2::new(405.0, offset), None);
            offset += 20.0;
        }

       TextCached::draw_queued(ctx, DrawParam::default())?;

       Ok(())
    }
}
