extern crate rand;

use std::mem;
use ship::Ship;
use ggez::*;
use ggez::graphics::*;
use radar::RadarMeasurement;

gfx_defines!{
    constant ShaderRadarData {
        target_range: [f32; 4] = "target_range",
        target_energy: [f32; 4] = "target_energy",
        ship_velocity: [f32; 2] = "ship_velocity",
        mount_direction: f32 = "mount_direction",
        antenna_direction: f32 = "beam_direction",
        beam_width: f32 = "beam_width",
        max_range: f32 = "max_range",
        min_angle: f32 = "min_angle",
        max_angle: f32 = "max_angle",
        random_value: f32 = "random_value",
    }
}

pub struct RadarRenderer {
    shader_data: ShaderRadarData,
    is_ppi_renderer: bool,
    gain: f64,
    shader: graphics::Shader<ShaderRadarData>,
    radar_display: graphics::Canvas,
    current_radar_display: graphics::Canvas,
}

impl RadarRenderer {
    //Max and min angle is ignored by ppi display
    pub fn new(ctx: &mut Context, max_range: f32, min_angle: f32, max_angle: f32, is_ppi_renderer: bool) -> GameResult<RadarRenderer> {
        let shader_data = ShaderRadarData{
            antenna_direction: 0.0,
            beam_width: 0.0,
            max_range,
            min_angle,
            max_angle,
            target_range: [0.0, 0.0, 0.0, 0.0],
            target_energy: [0.0, 0.0, 0.0, 0.0],
            ship_velocity: [1.0, 0.0],
            mount_direction: 0.0,
            random_value: 0.0,
        };
        let shader = if is_ppi_renderer {
            graphics::Shader::new(
                ctx,
                "/basic_150.glslv",
                "/ppi_radar_150.glslf",
                shader_data,
                "ShaderRadarData",
                None,
                )?}
            else {
            graphics::Shader::new(
                ctx,
                "/basic_150.glslv",
                "/b_radar_150.glslf",
                shader_data,
                "ShaderRadarData",
                None,
                )?};
        let radar_display = graphics::Canvas::with_window_size(ctx)?;
        let current_radar_display = graphics::Canvas::with_window_size(ctx)?;
        Ok(RadarRenderer{shader_data, shader, radar_display, current_radar_display, is_ppi_renderer, gain: 1.0})
    }

    pub fn update(&mut self, ctx: &mut Context, measurement: &RadarMeasurement, player: &Ship) -> GameResult<()> {
        self.shader_data.antenna_direction = measurement.antenna_direction as f32;
        self.shader_data.mount_direction = player.rotation as f32;
        self.shader_data.beam_width = measurement.beam_width as f32;
        self.shader_data.random_value = rand::random::<f32>();
        self.shader_data.ship_velocity = [player.l_velocity.x as f32, player.l_velocity.y as f32];

        let mut ref_iter = measurement.reflections.iter();
        for i in 0..3 {
            if let Some(s) = ref_iter.next(){
                self.shader_data.target_range[i] = s.distance as f32;
                let dist_ampl = if(s.distance.powi(4) < 8e10){8e10}else{s.distance.powi(4)};
                self.shader_data.target_energy[i] = (s.energy*dist_ampl) as f32;
            }
            else{
                self.shader_data.target_range[i] = 0.0;
                self.shader_data.target_energy[i] = 0.0;
            }
        }

        {
            let _lock = graphics::use_shader(ctx, &self.shader);
            self.shader.send(ctx, self.shader_data);
            graphics::set_canvas(ctx, Some(&self.radar_display));
            graphics::clear(ctx);
            graphics::draw(
                ctx,
                &self.current_radar_display,
                Point2::new(0.0, 0.0),
                0.0
                )?;
            graphics::set_canvas(ctx, None);
        }

        mem::swap(&mut self.radar_display, &mut self.current_radar_display);
        Ok(())
    }

    pub fn render_radar(&mut self, ctx: &mut Context, dest: graphics::Rect) -> GameResult<()> {
        let screen_size = get_drawable_size(ctx);
        graphics::draw_ex(
            ctx,
            &self.radar_display,
            DrawParam{
                dest: Point2::new(dest.x, dest.y),
                scale: Point2::new(dest.w/screen_size.0 as f32, dest.h/screen_size.1 as f32),
                ..
                Default::default()
            }
            )?;
        Ok(())
    }
}

