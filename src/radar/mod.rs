pub mod radar_renderer;

extern crate nalgebra;

use std::cmp::Ordering;
use std::f64::consts::PI;
use std::rc::Rc;

pub struct Radar {
    position: nalgebra::Vector2<f64>,
    power: f64,
    pulse_width: f64,
    frequency: f64,
    pub antenna: Antenna,
    pub last_mesurement: Option<RadarMeasurement>,
}

//All antennas are parabolic antennas
pub struct Antenna {
    pub direction: f64,
    pub mount_direction: f64,
    diameter: f64,
    aperture_efficiency: f64,
}

pub struct Reflection {
    pub distance: f64,
    pub energy: f64,
}

impl Reflection {
    pub fn new(distance: f64, energy: f64) -> Reflection {
        Reflection { distance, energy }
    }
}

pub struct RadarMeasurement {
    pub reflections: Vec<Reflection>,
    pub range: f64,
    pub beam_direction: f64,
    pub beam_width: f64,
    pub antenna_direction: f64,
}

#[derive(Debug)]
pub struct Shadow {
    pub beam_direction: f64,
    pub beam_width: f64,
    pub shadow_range: f64,
}

pub trait RadarTarget {
    fn get_reflection(
        &self,
        position: nalgebra::Vector2<f64>,
        beam_direction: f64,
        beam_width: f64,
        shadows: &Vec<Shadow>,
        reflection_const: f64,
    ) -> Option<Reflection>;
    fn get_shadow(
        &self,
        position: nalgebra::Vector2<f64>,
        beam_direction: f64,
        beam_width: f64,
    ) -> Option<Shadow>;
}

impl Radar {
    pub fn new(
        position: nalgebra::Vector2<f64>,
        antenna: Antenna,
        power: f64,
        pulse_width: f64,
        frequency: f64,
    ) -> Radar {
        Radar {
            position,
            last_mesurement: None,
            antenna,
            power,
            pulse_width,
            frequency,
        }
    }

    pub fn make_mesurement(&mut self, targets: &Vec<Rc<dyn RadarTarget>>, range: f64) {
        let mut shadows: Vec<Shadow> = Vec::new();
        let (mut beam_direction, beam_width) = (
            self.antenna.direction + self.antenna.mount_direction,
            self.antenna.get_beam_width(self.frequency),
        );
        if beam_direction > PI {
            beam_direction = beam_direction - 2.0 * PI;
        }
        if beam_direction < -PI {
            beam_direction = beam_direction + 2.0 * PI;
        }

        for ref target in targets {
            if let Some(s) = target.get_shadow(self.position, beam_direction, beam_width) {
                shadows.push(s);
            }
        }

        let wavelenght = 3e8 / self.frequency;
        let reflection_const = (self.power
            * self.pulse_width
            * self.antenna.get_gain(self.frequency).powi(2)
            * wavelenght.powi(2)) / (4.0 * PI).powi(3);

        let mut reflections: Vec<Reflection> = Vec::new();

        for ref target in targets {
            if let Some(r) = target.get_reflection(
                self.position,
                beam_direction,
                beam_width,
                &shadows,
                reflection_const,
            ) {
                reflections.push(r);
            }
        }

        self.last_mesurement = Some(RadarMeasurement {
            reflections,
            range,
            beam_direction,
            beam_width,
            antenna_direction: self.antenna.direction,
        });
    }

    pub fn set_position(&mut self, position: nalgebra::Vector2<f64>) {
        self.position = position;
    }
}

impl Antenna {
    pub fn new(direction: f64, diameter: f64) -> Antenna {
        Antenna {
            direction,
            diameter,
            aperture_efficiency: 0.7,
            mount_direction: super::std::f64::consts::PI / 2f64,
        }
    }

    pub fn rotate(&mut self, angle: f64) {
        self.direction += angle;
        if self.direction > PI {
            self.direction -= 2.0 * PI
        }
        if self.direction < -PI {
            self.direction += 2.0 * PI
        }
    }

    fn get_gain(&self, frequency: f64) -> f64 {
        ((PI * self.diameter * frequency) / 3e8).powi(2) * self.aperture_efficiency
    }

    fn get_beam_width(&self, frequency: f64) -> f64 {
        3e8 * 1.23 / (self.diameter * frequency)
    }
}
