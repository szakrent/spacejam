extern crate nalgebra;

use radar::*;
use std::f64::consts::PI;

pub struct Asteroid{
    pub position: nalgebra::Vector2<f64>,
    pub radius: f64,
}

impl Asteroid {
    pub fn new(position: nalgebra::Vector2<f64>, radius: f64) -> Asteroid{
        Asteroid{
            position,
            radius,
        }
    }
}

impl RadarTarget for Asteroid {
    fn get_reflection(&self, position: nalgebra::Vector2<f64>, beam_direction: f64,
                      beam_width: f64, shadows: &Vec<Shadow>, reflection_const: f64) -> Option<Reflection>{
        let dir_vec = self.position-position;
        let distance = dir_vec.norm();
        let real_angle = (dir_vec.y).atan2(dir_vec.x);
        let angular_size = 2.0*(self.radius/distance).atan();
        
        let mut angle_diff = (beam_direction-real_angle).abs();
        if angle_diff > PI {
            angle_diff = 2.0*PI-angle_diff;
        }
        let angle_diff = -(angle_diff).abs();
        let distance_offset = (self.radius*(PI-angle_diff+((distance*angle_diff.sin())/self.radius).asin()).sin())/(angle_diff.sin()).abs()-distance;
        let mut angle_diff = (angle_diff).abs();

        for ref shadow in shadows {
            let mut shadow_angle_diff = (shadow.beam_direction-beam_direction).abs();
            if shadow_angle_diff > PI {
                shadow_angle_diff = 2.0*PI-shadow_angle_diff;
            }
            if shadow_angle_diff-shadow.beam_width*0.5 < beam_width && shadow.shadow_range < distance+distance_offset {
                return None;
            }
        }

        if angle_diff-beam_width*0.5 < angular_size*0.5 {
            let reflection_coeff = if beam_width > angular_size {
                angular_size/beam_width
            }
            else if angle_diff > angular_size/2.0 {
                1.0-(angle_diff-angular_size/2.0)/beam_width
            }
            else{
                1.0
            };
            Some(Reflection::new(distance+distance_offset, reflection_coeff*reflection_const/distance.powi(4)))
        }
        else{
            None
        }
    }
    fn get_shadow(&self, position: nalgebra::Vector2<f64>, beam_direction: f64, beam_width: f64) -> Option<Shadow>{
        let dir_vec = self.position-position;
        let distance = dir_vec.norm();
        let real_angle = (dir_vec.y).atan2(dir_vec.x);
        let angular_size = 2.0*(self.radius/distance).atan();
        
        let mut angle_diff = (beam_direction-real_angle).abs();
        if angle_diff > PI {
            angle_diff = 2.0*PI-angle_diff;
        }
        let angle_diff = -(angle_diff).abs();
        let distance_offset = (self.radius*(PI-angle_diff+((distance*angle_diff.sin())/self.radius).asin()).sin())/(angle_diff.sin()).abs()-distance;
        let mut angle_diff = (angle_diff).abs();

        let res =Some(Shadow{
            beam_direction: real_angle, 
            beam_width: angular_size,
            shadow_range: distance+distance_offset,
        });
        res
    }
}
