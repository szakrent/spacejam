#version 150 core

uniform sampler2D t_Texture;
in vec2 v_Uv;
in vec4 v_Color;
out vec4 Target0;

layout (std140) uniform Globals {
	mat4 u_MVP;
};

layout (std140) uniform ShaderRadarData {
	//using vector because gfx_defines does not support arrays
	vec4 target_range;
	vec4 target_energy;
	vec2 ship_velocity;
        float mount_direction;
        float beam_direction;
        float beam_width;
        float max_range;
        float min_angle;
        float max_angle;
	float random_value;
};

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
	float angle = min_angle + abs(max_angle-min_angle)*v_Uv.x;
	float range = max_range*v_Uv.y;
	
	float target_coefficent = 0.0;
	//can't iterate over vector :(
	target_coefficent += clamp(target_coefficent + (1.0-(4/pow(10.0, 2))*pow(range-target_range.r, 2))*target_energy.r, 0.0, 1.0);
	target_coefficent += clamp(target_coefficent + (1.0-(4/pow(10.0, 2))*pow(range-target_range.g, 2))*target_energy.g, 0.0, 1.0);
	target_coefficent += clamp(target_coefficent + (1.0-(4/pow(10.0, 2))*pow(range-target_range.b, 2))*target_energy.b, 0.0, 1.0);
	target_coefficent += clamp(target_coefficent + (1.0-(4/pow(10.0, 2))*pow(range-target_range.a, 2))*target_energy.a, 0.0, 1.0);
	//noise
	target_coefficent += clamp(rand(vec2(angle+1, range+1)*random_value)*2e-12*pow(range, 4), 0.0, 1.0);
	
	float prev_intensity = texture(t_Texture, v_Uv).r;
	float decay_intensity = clamp(prev_intensity*0.96, 0.0, 1);
	float intensity = clamp(decay_intensity + clamp(target_coefficent , 0.05, 1.0) * clamp( 1.0-(4/pow(beam_width, 2)*pow(angle-beam_direction, 2)), 0.0, 1.0), 0.010, 1.0);
	Target0 = vec4(vec3(1.0, 0.75, 0.0)*intensity, 1.0);
}
